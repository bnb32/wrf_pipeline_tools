#!/bin/python
#
import numpy, os
import Nio
import Ngl
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema

data_dir1  = "/glade/p/cesmdata/inputdata/atm/cam/volc"
data_dir2 = "/glade/p_old/cesmLE/CESM-CAM5-BGC-LE/atm/proc/tseries/monthly/FSNTOA"
file1 = "/IVI2LoadingLatHeight501-2000_L18_c20100518.nc"
file2 = "/f.e11.F1850C5CN.f09_f09.001.cam.h0.FSNTOA.120001-129912.nc"
volc_dat = Nio.open_file(data_dir1+file1,"r")
#toa_dat = Nio.open_file(data_dir2+file2,"r")
#
#fsntoa_tmp = toa_dat.variables["FSNTOA"].get_value()
#toa_time = toa_dat.variables["time"].get_value()
#fsntoa = []
#
#for i in range(len(fsntoa_tmp)):
#    fsntoa.append(numpy.sum(fsntoa_tmp[i,:,:]))
#
#fsntoa_yrs = []
#toa_time_yrs = []
#
#for i in range(len(fsntoa)/12):
#    fsntoa_yrs.append(numpy.mean(fsntoa[12*i:12*(i+1)]))
#    toa_time_yrs.append(i)

lats = volc_dat.variables["lat"].get_value()
colmass_tmp = volc_dat.variables["colmass"].get_value()
time = volc_dat.variables["time"].get_value()
date = volc_dat.variables["date"].get_value()
colmass = []
#colmass_sort = []

blat_idx = (numpy.abs(lats+30.0)).argmin()
tlat_idx = (numpy.abs(lats-30.0)).argmin()

for i in range(len(colmass_tmp)):
    sum_val = numpy.sum(colmass_tmp[i,blat_idx:tlat_idx+1])
    colmass.append(sum_val)

colmass = numpy.array(colmass)
colmass_sort = colmass[argrelextrema(colmass, numpy.greater)[0]]
colmass_sort.sort()

max_erups = colmass_sort[-100:]
time_erups_sort = []
time_erups = []
lat_erups=[]

for i in range(len(max_erups)):
    idx=(numpy.abs(max_erups[i]-colmass[:])).argmin()
    time_erups_sort.append(time[idx])
    time_erups.append(time_erups_sort[i])
    lat_erups.append(lats[(numpy.abs(colmass_tmp[idx,:])).argmax()])


time_erups_sort.sort()
time_erups_filt = []
max_erups_filt = []
lat_erups_filt=[]

val = time_erups_sort[0]
time_erups_filt.append(int(val))
for i in range(len(time_erups_sort)-1):
    if time_erups_sort[i] > val + 2.0:
        val = time_erups_sort[i]
        time_erups_filt.append(val)

for i in range(len(time_erups_filt)):
    val = colmass[(numpy.abs(time[:]-time_erups_filt[-len(time_erups_filt)+i])).argmin()]
    max_erups_filt.append(val)
    lat_erups_filt.append(lat_erups[(numpy.abs(numpy.array(time_erups)-time_erups_filt[i])).argmin()])
    

#max_erups_filt=[2-x/max(max_erups_filt[-50:]) for x in max_erups_filt[-50:]]
max_erups_filt=[x for x in max_erups_filt[-50:]]
lat_erups_filt=[x for x in lat_erups_filt[-50:]]

for i in range(len(max_erups_filt)):
    max_erups_filt[i]=max_erups_filt[i]*(1-abs(15.-lat_erups_filt[i])/30.)

print(time_erups_filt[-50:])        
print(max_erups_filt)
print(lat_erups_filt[-50:])

lsize=15
tsize=20

plt.rc('font',size=lsize)
plt.rc('axes',titlesize=tsize)
plt.rc('axes',labelsize=lsize)
plt.rc('xtick',labelsize=lsize)
plt.rc('ytick',labelsize=lsize)

plt.locator_params(axis='y',nbins=6)

plt.title("Eruption Signals")
plt.xlabel("Year")
plt.ylabel("Aerosol Mass "+r'$(kg/m^2)$')
plt.plot(time,colmass)
plt.savefig("eruptions_plot.pdf",bbox_inches='tight',pad_inches=0)
#plt.clf()

#plt.xlabel("Day")
#plt.ylabel("FSNTOA")
#plt.plot(toa_time_yrs,fsntoa_yrs)
#plt.savefig("fsntoa_plot.pdf",bbox_inches='tight')
