#!/usr/bin/python
 
###############################################################################
#
#
###############################################################################

import numpy,sys,os

from diag_functions import *

from plot_functions import *

import matplotlib.pyplot as plt

#---------- Get Arguments ------------------------------------------

if len(sys.argv) < 4:
    print "usage "+sys.argv[0]+": <infile1> <infile2> <start_year> <end_year>"
    sys.exit(1)
else:
    tcfilename1 = sys.argv[1]
    tcfilename2 = sys.argv[2]
    figname = sys.argv[1]
    start_year = int(sys.argv[3])
    end_year = int(sys.argv[4])


#---------- diag params -------------------#

min_lat = 0
max_lat = 45
lat_res = 2
    
min_lon = -130
max_lon = 10
lon_res = 4

min_wind = 10.0
max_wind = 100.0
wind_res = 3.0

min_press = 900.0
max_press = 1020.0
press_res = 3.0

dur_days = 8

title = "Storm Number ( "+str(start_year)+"-"+str(end_year)+" )"

fig = plot_num_fig(tcfilename1, tcfilename2, start_year,end_year,
                 min_lat,max_lat,lat_res,min_lon,max_lon,lon_res,
		 min_wind,max_wind,wind_res,min_press,max_press,press_res,
		 dur_days,title=title)

plt.savefig(figname+"_diags.pdf",bbox_inches='tight')
