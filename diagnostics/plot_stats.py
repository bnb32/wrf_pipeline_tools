#!/usr/bin/python
 
###############################################################################
#
#
###############################################################################

import numpy,sys,os

from diag_functions import *

from plot_functions import *

import matplotlib.pyplot as plt

#---------- Get Arguments ------------------------------------------

if len(sys.argv) < 4:
    print "usage "+sys.argv[0]+": <infile> <compfile> <start_year> <end_year>"
    sys.exit(1)
else:
    tcfilename = sys.argv[1]
    compfile = sys.argv[2]
    figname = sys.argv[1]
    start_year = int(sys.argv[3])
    end_year = int(sys.argv[4])


#---------- diag params -------------------#

min_lat = 0
max_lat = 55
lat_res = 1
    
min_lon = -135
max_lon = 10
lon_res = 3

min_wind = 10.0
max_wind = 100.0
wind_res = 3.0

min_press = 850.0
max_press = 1020.0
press_res = 3.0

dur_days = 15

year_avg = 3
ref_syear = 1000
ref_eyear = 1100

title = "Storm Number ( "+str(start_year)+"-"+str(end_year)+" )"

outer_grid = gridspec.GridSpec(1,3,wspace=0.3,hspace=0.3)

fig = plot_num_fig(tcfilename,start_year,end_year,
                 min_lat,max_lat,lat_res,min_lon,max_lon,lon_res,
		 min_wind,max_wind,wind_res,min_press,max_press,press_res,
		 dur_days,outer_grid[0])

fig = plot_pval_fig(tcfilename,compfile,start_year,end_year,year_avg,
                 ref_syear,ref_eyear,
                 min_lat,max_lat,lat_res,min_lon,max_lon,lon_res,
		 min_wind,max_wind,wind_res,min_press,max_press,press_res,
		 dur_days,outer_grid[1],fig)

fig = plot_num_fig(compfile,ref_syear,ref_eyear,
                 min_lat,max_lat,lat_res,min_lon,max_lon,lon_res,
		 min_wind,max_wind,wind_res,min_press,max_press,press_res,
		 dur_days,outer_grid[2],fig)


all_axes = fig.get_axes()

for i in range(len(all_axes)):
    ax = all_axes[i]
    if i == 0:
        title = "Storm Number\n( Forced: "+str(start_year)+"-"+str(end_year)+" )"
        ax.text(1.3,1.4,title,size=8,horizontalalignment='center',transform=ax.transAxes)
    if i == 8:
        title = "P Value\n( "+str(start_year)+"-"+str(end_year)+" )"
        ax.text(1.3,1.4,title,size=8,horizontalalignment='center',transform=ax.transAxes)
    if i == 15:
        title = "Storm Number\n( Control: "+str(start_year)+"-"+str(end_year)+" )"
        ax.text(1.3,1.4,title,size=8,horizontalalignment='center',transform=ax.transAxes)

	
plt.savefig(figname+"_stats.pdf",bbox_inches='tight')
