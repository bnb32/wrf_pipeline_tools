#!/usr/bin/python
 
###############################################################################
#
#
###############################################################################

import numpy as np

import sys,os,subprocess

from diag_functions import *

from plot_functions import *

import matplotlib.pyplot as plt

import pandas

from comp_stats_data import *

if len(sys.argv)==6: tcl=int(sys.argv[1]); tcr=int(sys.argv[2]); print_data=sys.argv[3]; nrun=int(sys.argv[4]); run_control=sys.argv[5]
else: tcl=10; tcr=-50; print_data=False; nrun=9; run_control=False

tcfilename2 = "/glade/u/home/bbenton/TSTORMS_OUTPUT/ctrl/WRF/run_%s/traj_out_ctrl_950_1104_%s" %(nrun,nrun)

#start_years=[1213,1258,1275,1284,1452,1600,1641,1762,1809,1815]


start_years=[1188, 1213, 1232, 1258, 1268, 1275, 1284, 1307, 1316, 1328, 1341, 1358, 1370, 1381, 1416, 1452, 1459, 1474, 1480, 1503, 1512, 1526, 1534, 1584, 1593, 1600, 1619, 1641, 1673, 1693, 1711, 1719, 1729, 1738, 1756, 1762, 1794, 1796, 1809, 1815, 1831, 1835, 1862, 1883, 1886, 1903, 1911, 1925, 1963, 1976]

start_years=[x for x in start_years]
#end_years=[x for x in start_years]
end_years=[x+1 for x in start_years]

strengths=[0.0002844205, 0.0039387355, 0.0013156489, 0.018610299, 0.0014670904, 0.00577007, 0.0042582103, 0.00011168822, 0.00017321129, 0.0011022617, 0.0027219066, 0.00010316961, 0.0002691743, 0.0004988108, 0.00069950486, 0.01123978, 0.0012253568, 0.00031424107, 0.00039538497, 9.6238095e-05, 0.00023723794, 0.0001980713, 0.0003649701, 0.0013540474, 0.0009029704, 0.0035108852, 0.0004950247, 0.0035000157, 0.0012742055, 0.0025623553, 0.0003653527, 0.0017625, 0.0006352736, 0.00032004144, 0.00041724482, 0.004875931, 0.00017844126, 0.0003748809, 0.003996972, 0.0080638705, 0.0009511905, 0.0027239982, 0.00041456614, 0.0016446952, 0.00018493412, 0.00036901433, 0.00061599945, 0.00034734656, 0.0014599211, 0.00025356218]

#strengths=[0.0039387355, 0.018610299, 0.00577007, 0.0042582103, 0.01123978, 0.0035108852, 0.0035000157, 0.004875931, 0.003996972, 0.0080638705]

erup_lats=[15.3483647594915, 15.3483647594915, -4.18592053318915, 12.5577561152307, -4.18592053318915, -4.18592053318915, -4.18592053318915, -4.18592053318915, -4.18592053318915, 15.3483647594915, -4.18592053318915, -4.18592053318915, 15.3483647594915, -4.18592053318915, 12.5577561152307, -4.18592053318915, 15.3483647594915, -4.18592053318915, 15.3483647594915, 15.3483647594915, 15.3483647594915, 15.3483647594915, -4.18592053318915, 15.3483647594915, -4.18592053318915, 15.3483647594915, -4.18592053318915, 15.3483647594915, -4.18592053318915, -4.18592053318915, -4.18592053318915, 15.3483647594915, 15.3483647594915, -4.18592053318915, 15.3483647594915, 15.3483647594915, -4.18592053318915, 15.3483647594915, 4.18592053318916, 4.18592053318916, 15.3483647594915, 15.3483647594915, -4.18592053318915, 4.18592053318916, -4.18592053318915, -4.18592053318915, 15.3483647594915, 15.3483647594915, -4.18592053318915, 15.3483647594915]

weights=[2-x/max(strengths) for x in strengths] 

s_years=[]
e_years=[]
e_lats=[]

#strengths=[strengths[i]*(1.0-abs(erup_lats[i]-4.0)/15.0) for i in range(len(strengths))]

str_sort=sorted(strengths)
str_sort=str_sort[-tcl:-tcr]
for x in str_sort:
    idx=(np.abs(np.array(strengths)-x)).argmin()
    s_years.append(start_years[idx])
    e_years.append(end_years[idx])
    e_lats.append(erup_lats[idx])

start_years=s_years
end_years=e_years
strengths=str_sort
erup_lats=e_lats

forced_yrs=0
for i in range(len(start_years)):
    forced_yrs+=end_years[i]-start_years[i]+1

tcdir="/glade/u/home/bbenton/TSTORMS_OUTPUT/forced/WRF/run_%s" %(nrun)

lmon=5;rmon=11
llat=0.0;rlat=25.0
llon=-100.0;rlon=-50.0
lwind=0.0;rwind=40.0
lpress=1020.0;rpress=980.0
ltime=0.0;rtime=100.0
min_lat = 0;max_lat = 55;lat_res = 1    
min_lon = -135;max_lon = 10;lon_res = 3
#min_wind = 10.0;max_wind = 100.0;wind_res = 3.0
min_wind = 10.0;max_wind = 60.0;wind_res = 1.0
#min_press = 850.0;max_press = 1020.0;press_res = 3.0
min_press = 950.0;max_press = 1020.0;press_res = 1.0
#dur_days = 15
dur_days = 7

avg_store=[]
diff_store=[]
rat_store=[]

ref_syear=950
ref_eyear=1104
ctrl_yrs=ref_eyear-ref_syear+1
ref_avg=1

ref_dir="/glade/u/home/bbenton/TSTORMS_OUTPUT/ctrl/WRF/run_%s" %(nrun)

filename = "/glade/u/home/bbenton/TSTORMS_OUTPUT/forced/WRF/run_%s/traj_out_forced_%s_%s_%s"

command = "if [ ! -f %s ]; then for ((year=%s; year<=%s; year++)); do filename=%s/$year/traj_out_forced_$year; if [ -f $filename ]; then cat $filename >> %s; fi; done; fi"

if run_control:

    [ref_stds,ref_avgs,ref_mons,ref_years,ref_lats,ref_lons,ref_ints,ref_press,ref_wdurds,ref_pdurds]=get_ref_stats(tcfilename2,ref_dir,ref_syear,ref_eyear,ref_avg,
                      lmon,rmon,llat,rlat,llon,rlon,lwind,rwind,lpress,
    		  rpress,ltime,rtime,min_lat,max_lat,lat_res,
    		  min_lon,max_lon,lon_res,min_wind,max_wind,
    		  wind_res,min_press,max_press,press_res,dur_days)
    
    print("ref_stds=%s"%ref_stds)
    print("ref_avgs=%s"%ref_avgs)
    print("ref_mons=%s"%ref_mons)
    print("ref_years=%s"%ref_years)
    print("ref_lats=%s"%ref_lats)
    print("ref_lons=%s"%ref_lons)
    print("ref_ints=%s"%ref_ints)
    print("ref_press=%s"%ref_press)
    print("ref_wdurds=%s"%ref_wdurds)
    print("ref_pdurds=%s"%ref_pdurds)
    
    [months,monthly_num2,
    years2,yearly_num2,
    lats,lat_num2,
    lons,lon_num2,
    intsys,intsy_num2,
    press,pres_num2,
    durs,wdurd_num2,
    pdurd_num2] = get_all_diags(tcfilename2,ref_syear,ref_eyear,
                                     min_lat,max_lat,lat_res,
            			     min_lon,max_lon,lon_res,
            			     min_wind,max_wind,wind_res,
            			     min_press,max_press,press_res,
            			     dur_days)
    
    [monthly_num2,
    yearly_num2,
    lat_num2,
    lon_num2,
    intsy_num2,
    pres_num2,
    wdurd_num2,
    pdurd_num2] = get_ref_dists(tcfilename2,ref_dir,ref_syear,ref_eyear,ref_avg,
                                     min_lat,max_lat,lat_res,
            			     min_lon,max_lon,lon_res,
            			     min_wind,max_wind,wind_res,
            			     min_press,max_press,press_res,
            			     dur_days)
    
    
    yn2_hist=np.histogram(yearly_num2,bins=30)
    yearly_num2=yn2_hist[0]
    
    print("yr_bins=%s"%list(yn2_hist[1]))
    print("monthly_num2=%s"%list(monthly_num2))
    print("yearly_num2=%s"%list(yearly_num2))
    print("lat_num2=%s"%list(lat_num2))
    print("lon_num2=%s"%list(lon_num2))
    print("intsy_num2=%s"%list(intsy_num2))
    print("pres_num2=%s"%list(pres_num2))
    print("wdurd_num2=%s"%list(wdurd_num2))
    print("pdurd_num2=%s"%list(pdurd_num2))
    
    exit()

[avg_store,diff_store,rat_store,monthly_num,yearly_num,lat_num,
 lon_num,intsy_num,pres_num,wdurd_num,pdurd_num,months,lats,lons,press,intsys,durs]=get_samp_stats(filename,command,tcdir,ref_avgs,ref_stds,nrun,yr_bins,start_years,end_years,min_lat,max_lat,lat_res,min_lon,max_lon,lon_res,min_wind,max_wind,wind_res,min_press,max_press,press_res,dur_days,lmon,rmon,llat,rlat,llon,rlon,lwind,rwind,lpress,rpress,ltime,rtime)

monthly_num=monthly_num/float(forced_yrs)
monthly_num2=np.asarray(monthly_num2)/float(ctrl_yrs)
yearly_num=yearly_num#/float(forced_yrs)#float(np.sum(yearly_num))
yearly_num2=np.asarray(yearly_num2)#/float(ctrl_yrs)#float(np.sum(yearly_num2))
lat_num=lat_num/float(forced_yrs)
lat_num2=np.asarray(lat_num2)/float(ctrl_yrs)
lon_num=lon_num/float(forced_yrs)
lon_num2=np.asarray(lon_num2)/float(ctrl_yrs)
intsy_num=intsy_num/float(forced_yrs)
intsy_num2=np.asarray(intsy_num2)/float(ctrl_yrs)
pres_num=pres_num/float(forced_yrs)
pres_num2=np.asarray(pres_num2)/float(ctrl_yrs)
wdurd_num=wdurd_num/float(forced_yrs)
wdurd_num2=np.asarray(wdurd_num2)/float(ctrl_yrs)
pdurd_num=pdurd_num/float(forced_yrs)
pdurd_num2=np.asarray(pdurd_num2)/float(ctrl_yrs)

'''

monthly_num=normed(monthly_num)
monthly_num2=normed(monthly_num2)
yearly_num=normed(yearly_num)#/float(forced_yrs)#float(np.sum(yearly_num))
yearly_num2=normed(yearly_num2)#/float(ctrl_yrs)#float(np.sum(yearly_num2))
lat_num=normed(lat_num)
lat_num2=normed(lat_num2)
lon_num=normed(lon_num)
lon_num2=normed(lon_num2)
intsy_num=normed(intsy_num)
intsy_num2=normed(intsy_num2)
pres_num=normed(pres_num)
pres_num2=normed(pres_num2)
wdurd_num=normed(wdurd_num)
wdurd_num2=normed(wdurd_num2)
pdurd_num=normed(pdurd_num)
pdurd_num2=normed(pdurd_num2)
'''

mons_rat=[rat_store[x][0] for x in range(len(rat_store))]
yrs_rat=[rat_store[x][1] for x in range(len(rat_store))]
lats_rat=[rat_store[x][2] for x in range(len(rat_store))]
lons_rat=[rat_store[x][3] for x in range(len(rat_store))]
intsys_rat=[rat_store[x][4] for x in range(len(rat_store))]
press_rat=[rat_store[x][5] for x in range(len(rat_store))]
wdurs_rat=[rat_store[x][6] for x in range(len(rat_store))]
pdurs_rat=[rat_store[x][7] for x in range(len(rat_store))]
pmons_rat=[rat_store[x][8] for x in range(len(rat_store))]
plats_rat=[rat_store[x][9] for x in range(len(rat_store))]
plons_rat=[rat_store[x][10] for x in range(len(rat_store))]
pints_rat=[rat_store[x][11] for x in range(len(rat_store))]
ppress_rat=[rat_store[x][12] for x in range(len(rat_store))]
pwdurs_rat=[rat_store[x][13] for x in range(len(rat_store))]
ppdurs_rat=[rat_store[x][14] for x in range(len(rat_store))]

mons_avg=[avg_store[x][0] for x in range(len(avg_store))]
yrs_avg=[avg_store[x][1] for x in range(len(avg_store))]
lats_avg=[avg_store[x][2] for x in range(len(avg_store))]
lons_avg=[avg_store[x][3] for x in range(len(avg_store))]
intsys_avg=[avg_store[x][4] for x in range(len(avg_store))]
press_avg=[avg_store[x][5] for x in range(len(avg_store))]
wdurs_avg=[avg_store[x][6] for x in range(len(avg_store))]
pdurs_avg=[avg_store[x][7] for x in range(len(avg_store))]
pmons_avg=[avg_store[x][8] for x in range(len(avg_store))]
plats_avg=[avg_store[x][9] for x in range(len(avg_store))]
plons_avg=[avg_store[x][10] for x in range(len(avg_store))]
pints_avg=[avg_store[x][11] for x in range(len(avg_store))]
ppress_avg=[avg_store[x][12] for x in range(len(avg_store))]
pwdurs_avg=[avg_store[x][13] for x in range(len(avg_store))]
ppdurs_avg=[avg_store[x][14] for x in range(len(avg_store))]

print("\nks-tests")
print("month & %s & %s \\\\" %(round(ks_test(monthly_num,monthly_num2)[0],3),round(ks_test(monthly_num,monthly_num2)[1],2)))
print("yearly num & %s & %s \\\\" %(round(ks_test(yearly_num,yearly_num2)[0],3),round(ks_test(yearly_num,yearly_num2)[1],2)))
print("lats & %s & %s \\\\" %(round(ks_test(lat_num,lat_num2)[0],3),round(ks_test(lat_num,lat_num2)[1],2)))
print("lons & %s & %s \\\\" %(round(ks_test(lon_num,lon_num2)[0],3),round(ks_test(lon_num,lon_num2)[1],2)))
print("max wind & %s & %s \\\\" %(round(ks_test(intsy_num,intsy_num2)[0],3),round(ks_test(intsy_num,intsy_num2)[1],2)))
print("min press & %s & %s \\\\" %(round(ks_test(pres_num,pres_num2)[0],3),round(ks_test(pres_num,pres_num2)[1],2)))
print("w-life & %s & %s \\\\" %(round(ks_test(wdurd_num,wdurd_num2)[0],3),round(ks_test(wdurd_num,wdurd_num2)[1],2)))
print("p-life & %s & %s \\\\" %(round(ks_test(pdurd_num,pdurd_num2)[0],3),round(ks_test(pdurd_num,pdurd_num2)[1],2)))

#print(erup_lats)

'''

print("\nmonth avg: %s" %(np.mean(mons_avg)))
print("yearly num avg: %s" %(np.mean(yrs_avg)))
print("lat avg: %s" %(np.mean(lats_avg)))
print("lon avg: %s" %(np.mean(lons_avg)))
print("max wind avg: %s" %(np.mean(intsys_avg)))
print("min press avg: %s" %(np.mean(press_avg)))
print("w-life avg: %s" %(np.mean(wdurs_avg)))
print("p-life avg: %s" %(np.mean(pdurs_avg)))
'''

forced_avgs=[avg_hori(months,monthly_num),np.mean(yrs_avg),avg_hori(lats,lat_num), avg_hori(lons,lon_num), avg_hori(intsys,intsy_num), avg_hori(press,pres_num), avg_hori(durs,wdurd_num), avg_hori(durs,pdurd_num)]

print("\nforced avgs")
print("month avg: %s" %(avg_hori(months,monthly_num)))
print("yearly num avg: %s" %(np.mean(yrs_avg)))
print("lat avg: %s" %(avg_hori(lats,lat_num)))
print("lon avg: %s" %(avg_hori(lons,lon_num)))
print("max wind avg: %s" %(avg_hori(intsys,intsy_num)))
print("min press avg: %s" %(avg_hori(press,pres_num)))
print("w-life avg: %s" %(avg_hori(durs,wdurd_num)))
print("p-life avg: %s" %(avg_hori(durs,pdurd_num)))

print("\nctrl avgs")
print("month avg: %s" %(ref_avgs[0]))
print("yearly num avg: %s" %(ref_avgs[1]))
print("lat avg: %s" %(ref_avgs[2]))
print("lon avg: %s" %(ref_avgs[3]))
print("max wind avg: %s" %(ref_avgs[4]))
print("min press avg: %s" %(ref_avgs[5]))
print("w-life avg: %s" %(ref_avgs[6]))
print("p-life avg: %s" %(ref_avgs[7]))

print("\ntotal diff: %s" %(total_uncert(forced_avgs,ref_avgs[0:8])))

'''
print("\nmonth diff: %s" %(np.mean(mons_rat)))
print("yearly num diff: %s" %(np.mean(yrs_rat)))
print("lat diff: %s" %(np.mean(lats_rat)))
print("lon diff: %s" %(np.mean(lons_rat)))
print("max wind diff: %s" %(np.mean(intsys_rat)))
print("min press diff: %s" %(np.mean(press_rat)))
print("w-life diff: %s" %(np.mean(wdurs_rat)))
print("p-life diff: %s" %(np.mean(pdurs_rat)))
'''
print("\nmonth diff: %s" %((avg_hori(months,monthly_num)-ref_avgs[0])/ref_stds[0]))
print("yearly num diff: %s" %((np.mean(yrs_avg)-ref_avgs[1])/ref_stds[1]))
print("lat diff: %s" %((avg_hori(lats,lat_num)-ref_avgs[2])/ref_stds[2]))
print("lon diff: %s" %((avg_hori(lons,lon_num)-ref_avgs[3])/ref_stds[3]))
print("max wind diff: %s" %((avg_hori(intsys,intsy_num)-ref_avgs[4])/ref_stds[4]))
print("min press diff: %s" %((avg_hori(press,pres_num)-ref_avgs[5])/ref_stds[5]))
print("w-life diff: %s" %((avg_hori(durs,wdurd_num)-ref_avgs[6])/ref_stds[6]))
print("p-life diff: %s" %((avg_hori(durs,pdurd_num)-ref_avgs[7])/ref_stds[7]))
'''
print("\nmonth sig: %s" %(sig_calc(np.mean(mons_avg),ref_mons)))
print("yearly num sig: %s" %(sig_calc(np.mean(yrs_avg),ref_years)))
print("lat sig: %s" %(sig_calc(np.mean(lats_avg),ref_lats)))
print("lon sig: %s" %(sig_calc(np.mean(lons_avg),ref_lons)))
print("max wind sig: %s" %(sig_calc(np.mean(intsys_avg),ref_ints)))
print("min press sig: %s" %(sig_calc(np.mean(press_avg),ref_press)))
print("w-life sig: %s" %(sig_calc(np.mean(wdurs_avg),ref_wdurds)))
print("p-life sig: %s" %(sig_calc(np.mean(pdurs_avg),ref_pdurds)))
'''

print("\nsig calcs")
print("month & %s & %s \\\\" %(round(sig_calc(avg_hori(months,monthly_num),ref_mons)[0],3),round(sig_calc(avg_hori(months,monthly_num),ref_mons)[1],3)))
print("yearly num & %s & %s \\\\" %(round(sig_calc(np.mean(yrs_avg),ref_years)[0],3),round(sig_calc(np.mean(yrs_avg),ref_years)[1],3)))
print("lats & %s & %s \\\\" %(round(sig_calc(avg_hori(lats,lat_num),ref_lats)[0],3),round(sig_calc(avg_hori(lats,lat_num),ref_lats)[1],3)))
print("lons & %s & %s \\\\" %(round(sig_calc(avg_hori(lons,lon_num),ref_lons)[0],3),round(sig_calc(avg_hori(lons,lon_num),ref_lons)[1],3)))
print("max wind & %s & %s \\\\" %(round(sig_calc(avg_hori(intsys,intsy_num),ref_ints)[0],3),round(sig_calc(avg_hori(intsys,intsy_num),ref_ints)[1],3)))
print("min press & %s & %s \\\\" %(round(sig_calc(avg_hori(press,pres_num),ref_press)[0],3),round(sig_calc(avg_hori(press,pres_num),ref_press)[1],3)))
print("w-life & %s & %s \\\\" %(round(sig_calc(avg_hori(durs,wdurd_num),ref_wdurds)[0],3),round(sig_calc(avg_hori(durs,wdurd_num),ref_wdurds)[1],3)))
print("p-life & %s & %s \\\\" %(round(sig_calc(avg_hori(durs,pdurd_num),ref_pdurds)[0],3),round(sig_calc(avg_hori(durs,pdurd_num),ref_pdurds)[1],3)))

if str(print_data)=="True":
    print("\nerup years: %s\n" %(start_years))
    print("erup strengths: %s\n" %(strengths))
    print("month diff: %s\n" %(mons_rat))
    print("yearly num diff: %s\n" %(yrs_rat))
    print("lat diff: %s\n" %(lats_rat))
    print("lon diff: %s\n" %(lons_rat))
    print("max wind diff: %s\n" %(intsys_rat))
    print("min press diff: %s\n" %(press_rat))
    print("w-life diff: %s\n" %(wdurs_rat))
    print("p-life diff: %s\n" %(pdurs_rat))


if len(strengths)>1:

    print("\ncorrelations")
    print("month & %s \\\\" %round(np.corrcoef(strengths,mons_rat)[0][1],4))
    print("yearly num & %s \\\\" %round(np.corrcoef(strengths,yrs_rat)[0][1],4))
    print("lats & %s \\\\" %round(np.corrcoef(strengths,lats_rat)[0][1],4))
    print("lons & %s \\\\" %round(np.corrcoef(strengths,lons_rat)[0][1],4))
    print("max wind & %s \\\\" %round(np.corrcoef(strengths,intsys_rat)[0][1],4))
    print("min press & %s \\\\" %round(np.corrcoef(strengths,press_rat)[0][1],4))
    print("w-life & %s \\\\" %round(np.corrcoef(strengths,wdurs_rat)[0][1],4))
    print("p-life & %s \\\\" %round(np.corrcoef(strengths,pdurs_rat)[0][1],4))
    '''
    "mons %",np.corrcoef(strengths,pmons_rat)[0][1],
    "lats %",np.corrcoef(strengths,plats_rat)[0][1],
    "lons %",np.corrcoef(strengths,plons_rat)[0][1],
    "wind %",np.corrcoef(strengths,pints_rat)[0][1],
    "press %",np.corrcoef(strengths,ppress_rat)[0][1],
    "w-life %",np.corrcoef(strengths,pwdurs_rat)[0][1],
    "p-life %",np.corrcoef(strengths,ppdurs_rat)[0][1]]
    '''

yrs_plot=(np.asarray(yr_bins[:-1])+np.asarray(yr_bins[1:]))
yrs_plot=yrs_plot/2.0

fig_rows=4
fig_cols=2

plt.rc('font',size=9)
plt.rc('axes', titlesize=10)

fig=plt.figure(1)
outer_grid=gridspec.GridSpec(1,1,wspace=0.0,hspace=0.0)[0]
inner_grid = gridspec.GridSpecFromSubplotSpec(fig_rows, fig_cols,
              subplot_spec=outer_grid, wspace=0.6, hspace=0.9)

fig.suptitle("Storm Number vs")

ax = plt.Subplot(fig,inner_grid[0])
l1,l2=ax.plot(months,monthly_num,'r',months,monthly_num2,'b')
fig.add_subplot(ax)
plt.title("Month")
plt.xlim([1,12])

ax = plt.Subplot(fig,inner_grid[1])
ax.plot(yrs_plot,yearly_num,'r',yrs_plot,yearly_num2,'b')
fig.add_subplot(ax)
plt.title("Years")
#plt.xlim([start_year,end_year])

ax = plt.Subplot(fig,inner_grid[2])
ax.plot(lats,lat_num,'r',lats,lat_num2,'b')
fig.add_subplot(ax)
plt.title("Latitude")
plt.xlim([min_lat,max_lat])

ax = plt.Subplot(fig,inner_grid[3])
ax.plot(lons,lon_num,'r',lons,lon_num2,'b')
fig.add_subplot(ax)
plt.title("Longitude")
plt.xlim([min_lon,max_lon])

ax = plt.Subplot(fig,inner_grid[4])
ax.plot(intsys,intsy_num,'r',intsys,intsy_num2,'b')
fig.add_subplot(ax)
plt.title("Wind Speed (m/s)")
plt.xlim([min_wind,max_wind])

ax = plt.Subplot(fig,inner_grid[5])
ax.plot(press[::-1],pres_num[::-1],'r',press[::-1],pres_num2[::-1],'b')
fig.add_subplot(ax)
plt.title("Pressure (mb)")
plt.xlim([min_press,max_press])

ax = plt.Subplot(fig,inner_grid[6])
ax.plot(durs,wdurd_num,'r',durs,wdurd_num2,'b')
fig.add_subplot(ax)
plt.title("Wind Life (hrs)")
plt.xlim([0,6*len(wdurd_num)])

ax = plt.Subplot(fig,inner_grid[7])
ax.plot(durs,pdurd_num,'r',durs,pdurd_num2,'b')
fig.add_subplot(ax)
plt.title("Press Life (hrs)")
plt.xlim([0,6*len(pdurd_num)])

fig.legend((l1,l2),('$LME_{forced}$','$LME_{control}$'),'upper left')

plt.savefig("f_vs_c_dists.pdf")
plt.clf()

'''
plt.rc('font',size=6)

plt.subplot(7,2,1)
plt.hist(mons_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(mons_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("months")

plt.subplot(7,2,2)
plt.hist(yrs_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(yrs_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("years")

plt.subplot(7,2,3)
plt.hist(lats_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(lats_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("lats")

plt.subplot(7,2,4)
plt.hist(lons_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(lons_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("lons")

plt.subplot(7,2,5)
plt.hist(intsys_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(intsys_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("wind")

plt.subplot(7,2,6)
plt.hist(press_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(press_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("pressure")

plt.subplot(7,2,7)
plt.hist(pdurs_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(pdurs_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("p-life")

plt.subplot(7,2,8)
plt.hist(wdurs_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(wdurs_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("w-life")

#plt.subplot(7,2,9)
#plt.hist(pmons_rat,bins=10)
#plt.annotate(r'$\mu=$'+str(round(np.mean(pmons_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
#plt.title("mon %")

plt.subplot(7,2,9)
plt.hist(plats_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(plats_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("lat %")

plt.subplot(7,2,10)
plt.hist(plons_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(plons_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("lon %")

plt.subplot(7,2,11)
plt.hist(pints_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(pints_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("wind %")

plt.subplot(7,2,12)
plt.hist(ppress_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(ppress_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("press %")

plt.subplot(7,2,13)
plt.hist(pwdurs_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(pwdurs_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("w-life %")

plt.subplot(7,2,14)
plt.hist(ppdurs_rat,bins=10)
plt.annotate(r'$\mu=$'+str(round(np.mean(ppdurs_rat),2)),xy=(0.05,0.75),xycoords='axes fraction')
plt.title("p-life %")

plt.subplots_adjust(top=0.9,bottom=0.15,left=0.2,right=0.9,hspace=1.4,wspace=0.7)

plt.savefig("diff_rats_hists.pdf")
plt.clf()

plt.rc('font',size=6)

plt.subplot(7,2,1)
plt.plot(start_years,mons_rat)
plt.title("months")

plt.subplot(7,2,2)
plt.plot(start_years,yrs_rat)
plt.title("years")

plt.subplot(7,2,3)
plt.plot(start_years,lats_rat)
plt.title("lats")

plt.subplot(7,2,4)
plt.plot(start_years,lons_rat)
plt.title("lons")

plt.subplot(7,2,5)
plt.plot(start_years,intsys_rat)
plt.title("wind")

plt.subplot(7,2,6)
plt.plot(start_years,press_rat)
plt.title("pressure")

plt.subplot(7,2,7)
plt.plot(start_years,pdurs_rat)
plt.title("p-life")

plt.subplot(7,2,8)
plt.plot(start_years,wdurs_rat)
plt.title("w-life")

#plt.subplot(7,2,9)
#plt.plot(start_years,pmons_rat)
#plt.title("mon %")

plt.subplot(7,2,9)
plt.plot(start_years,plats_rat)
plt.title("lat %")

plt.subplot(7,2,10)
plt.plot(start_years,plons_rat)
plt.title("lon %")

plt.subplot(7,2,11)
plt.plot(start_years,pints_rat)
plt.title("wind %")

plt.subplot(7,2,12)
plt.plot(start_years,ppress_rat)
plt.title("press %")

plt.subplot(7,2,13)
plt.plot(start_years,pwdurs_rat)
plt.title("w-life %")

plt.subplot(7,2,14)
plt.plot(start_years,ppdurs_rat)
plt.title("p-life %")

plt.subplots_adjust(top=0.9,bottom=0.15,left=0.2,right=0.9,hspace=1.4,wspace=0.7)

plt.savefig("diff_rats_trends.pdf")
plt.clf()

#plt.plot(start_years,strengths)
#plt.title("eruption strengths")
#plt.savefig("eruption_strengths.pdf")
'''
